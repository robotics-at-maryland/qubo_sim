SHELL = /bin/bash
.PHONY: all
all: help

# PUSH_CMD = rsync --exclude '/.git' --exclude '/build' --exclude '/install' --exclude '/log' -varP . qubo@192.168.20.10:/home/qubo/ros_ws/src/qubo/

COLCON_BUILD = colcon build --cmake-args -DBUILD_TESTING=ON

define HELP_MESSAGE
Utilities:
  build  	Builds docker container
  enter		Enter docker container
  run-sim	Runs headless simulation, must be in docker container
endef

.PHONY: help
export HELP_MESSAGE
help:
	@echo "$$HELP_MESSAGE"

.PHONY: build
build:
	docker build -t qubo .

.PHONY: enter
enter:
ifeq ($(shell docker images -q qubo 2> /dev/null),)
	@echo "Run: make build" 
endif
ifeq ($(shell echo $$DOCKER_RUNNING 2> /dev/null),)
	@docker run --rm -it \	
		-e DISPLAY=host.docker.internal:0 \
		-v $$(pwd):/ros_ws/src/qubo_sim  \
		-v /tmp/.X11-unix/:/tmp/.X11-unix/ \
		qubo sh -c "cd /ros_ws/src/qubo_sim && /bin/bash"
endif

.PHONY: run-sim
run-sim:
ifeq ($(shell docker images -q qubo 2> /dev/null),)
	@echo "Run: make build" 
endif
ifeq ($(shell echo $$DOCKER_RUNNING 2> /dev/null),)
	@chmod +x run-sim.sh
	@docker run --rm -it \
		-e DISPLAY=host.docker.internal:0 \
		-v $$(pwd):/ros_ws/src/qubo_sim \
		-v /tmp/.X11-unix/:/tmp/.X11-unix/\
		qubo /ros_ws/src/qubo_sim/run-sim.sh
endif
# ifeq ($(shell echo $$DOCKER_RUNNING 2> /dev/null),)
# 	echo "Run: make enter"
# else
# 	cd /ros_ws/src/qubo_sim
# 	rosdep install --from-paths src --ignore-src -r -i -y --rosdistro $${ROS_DISTRO}
# 	$(COLCON_BUILD)
# 	source ./install/setup.sh
# 	xvfb-run -a ros2 launch qubo_sim_bringup nbrf.launch.py
# endif