## Installation
Clone the repository and submodules:
```
git clone --recurse-submodules https://code.umd.edu/robotics-at-maryland/qubo_sim.git
```

Install Gazebo Garden and dependencies
```
cd qubo_sim
sudo apt-get update
sudo apt-get install lsb-release curl gnupg
sudo curl https://packages.osrfoundation.org/gazebo.gpg --output /usr/share/keyrings/pkgs-osrf-archive-keyring.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/pkgs-osrf-archive-keyring.gpg] http://packages.osrfoundation.org/gazebo/ubuntu-stable $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/gazebo-stable.list > /dev/null
sudo apt-get update
sudo apt-get install gz-garden
sudo apt-get install ros-humble-ros-gzgarden
rosdep update
rosdep install --from-paths src --ignore-src -r -y
```

Build:
```
GZ_VERSION=garden colcon build --cmake-args -DBUILD_TESTING=ON
```

Source command
(IMPORTANT! Don't forget this step.
You must rebuild and rerun source command every time you change sim files)
```
source install/setup.sh
```

Launch the sim
```
ros2 launch qubo_sim_bringup nbrf.launch.py
```
