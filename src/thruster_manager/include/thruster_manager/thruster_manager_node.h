#ifndef THRUSTER_MANAGER_THRUSTER_MANAGER_NODE_H
#define THRUSTER_MANAGER_THRUSTER_MANAGER_NODE_H

#include <rclcpp/node.hpp>
#include "rclcpp/timer.hpp"
#include <sensor_msgs/msg/joint_state.hpp>
#include <std_msgs/msg/float64.hpp>
#include <geometry_msgs/msg/wrench.hpp>
#include <geometry_msgs/msg/wrench_stamped.hpp>
#include <std_msgs/msg/string.hpp>
#include <std_msgs/msg/empty.hpp>
#include <thruster_manager/thruster_manager.h>

#include <unordered_map>
#include <string>

using std::placeholders::_1;

namespace thruster_manager
{

class ThrusterManagerNode : public rclcpp::Node
{
  using Float64 = std_msgs::msg::Float64;
  using JointState = sensor_msgs::msg::JointState;
  using Wrench = geometry_msgs::msg::Wrench;
  using WrenchStamped = geometry_msgs::msg::WrenchStamped;
  using String = std_msgs::msg::String;
  using Empty = std_msgs::msg::Empty;

public:
  ThrusterManagerNode(rclcpp::NodeOptions options);

private:

  ThrusterManager allocator;
  size_t dofs;

  void add_desired_thrust_callback(const WrenchStamped::SharedPtr msg);
  void remove_desired_thrust_callback(const String::SharedPtr msg);
  void combine_desired_thrusts();
  void reset_desired_thrusts_callback(const Empty::SharedPtr msg);

  rclcpp::Subscription<WrenchStamped>::SharedPtr add_desired_thrust_sub;
  rclcpp::Subscription<String>::SharedPtr remove_desired_thrust_sub;
  rclcpp::TimerBase::SharedPtr combine_desired_thrusts_timer;
  rclcpp::Subscription<Empty>::SharedPtr reset_desired_thrusts_sub;
  void solve(const Wrench &wrench);

  // if we publish output as joint states
  sensor_msgs::msg::JointState cmd;
  rclcpp::Publisher<sensor_msgs::msg::JointState>::SharedPtr cmd_js_pub;

  // if we publish output as Float64 (Gazebo thruster plugin)
  std::vector<rclcpp::Publisher<Float64>::SharedPtr> cmd_gz_pub;

  std::unordered_map<std::string, std::pair<rclcpp::Time, ThrusterManager::Vector6d>> desired_thrusts;

};
}


#endif
