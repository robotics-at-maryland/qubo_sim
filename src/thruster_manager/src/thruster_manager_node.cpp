#include <thruster_manager/thruster_manager_node.h>

using namespace thruster_manager;

rcl_interfaces::msg::ParameterDescriptor description(const std::string &des)
{
  return rcl_interfaces::msg::ParameterDescriptor().set__description(des);
}

ThrusterManagerNode::ThrusterManagerNode(rclcpp::NodeOptions options)
  : Node("thruster_manager", options)
{

  const auto pub_js{declare_parameter("publish_joint_state", true,  description("If the command should be published as joint state"))};
  const auto pub_gz{declare_parameter("publish_gz_command", true, description("If the command should be published as Float64 (Gazebo thruster plugin)"))};
  // const auto sub_stamped{declare_parameter("subscribe_stamped", false,
  //                                          description("If the node should expect WrenchStamped messages instead of Wrench"))};
  const auto control_frame{declare_parameter<std::string>("control_frame", "base_link")};

  // joint names are stored here anyway to sync joint names and indices
  const auto links{allocator.parseRobotDescription(this, control_frame)};
  std::transform(links.begin(), links.end(), std::back_inserter(cmd.name), [](const auto &link){return link.joint;});
  dofs = cmd.name.size();
  cmd.effort.resize(dofs);

  add_desired_thrust_sub = this->create_subscription<WrenchStamped>(
      "/desired_thrust/add",
      10,
      std::bind(&ThrusterManagerNode::add_desired_thrust_callback, this, _1)
  );
  remove_desired_thrust_sub = this->create_subscription<String>(
      "/desired_thrust/remove",
      10,
      std::bind(&ThrusterManagerNode::remove_desired_thrust_callback, this, _1)
  );
  combine_desired_thrusts_timer = this->create_wall_timer(
      std::chrono::milliseconds(20),
      std::bind(&ThrusterManagerNode::combine_desired_thrusts, this)
  );
  reset_desired_thrusts_sub = this->create_subscription<Empty>(
      "/desired_thrust/reset",
      10,
      std::bind(&ThrusterManagerNode::reset_desired_thrusts_callback, this, _1)
  );
  desired_thrusts = {};

  if(pub_js)
    cmd_js_pub = create_publisher<JointState>("cmd_thrust", 5);

  if(pub_gz)
  {
    for(const auto &name: cmd.name)
    {
      const auto topic{"cmd_" + name};
      cmd_gz_pub.push_back(create_publisher<Float64>(topic,5));
    }
  }

  // if(sub_stamped)
  // {
  //   wrench_sub = create_subscription<WrenchStamped>("wrench", 5, [&](const WrenchStamped::SharedPtr msg)
  //   {
  //     solve(msg->wrench);
  //   });
  // }
  // else
  // {
  //   wrench_sub = create_subscription<Wrench>("wrench", 5, [&](const Wrench::SharedPtr msg)
  //   {
  //     solve(*msg);
  //   });
  // }
}

void ThrusterManagerNode::add_desired_thrust_callback(const WrenchStamped::SharedPtr msg) {
    ThrusterManager::Vector6d v;
    Wrench wrench = msg->wrench;
    v << wrench.force.x, wrench.force.y, wrench.force.z,
         wrench.torque.x, wrench.torque.y, wrench.torque.z;
    desired_thrusts[msg->header.frame_id] = std::pair<rclcpp::Time, ThrusterManager::Vector6d>(
        Node::get_clock()->now(),
        v
    );
}

void ThrusterManagerNode::remove_desired_thrust_callback(const String::SharedPtr msg) {
    if (!desired_thrusts.erase(msg->data)) {
        RCLCPP_INFO(this->get_logger(),
            "remove_desired_thrust: invalid thrust %s", msg->data.c_str()
        );
    }
}

void ThrusterManagerNode::combine_desired_thrusts() {
    ThrusterManager::Vector6d F = {0, 0, 0, 0, 0, 0};
    std::vector<std::string> expired_thrusts;
    for (auto& pair : desired_thrusts) {
        if (pair.second.first < Node::get_clock()->now() - rclcpp::Duration(1, 0)) {
            RCLCPP_INFO(this->get_logger(),
                "combine_desired_thrusts: removing thrust %s last published at %f", pair.first.c_str(), pair.second.first.seconds()
            );
            expired_thrusts.push_back(pair.first);
        } else {
            RCLCPP_INFO(this->get_logger(),
                "combine_desired_thrusts: adding thrust %s last published at %f", pair.first.c_str(), pair.second.first.seconds()
            );
            F += pair.second.second;
        }
    }

    for (const auto& thrust : expired_thrusts) {
        desired_thrusts.erase(thrust);
    }

    std::stringstream debug_final_thrust_vector;
    for (int i = 0; i < F.size(); i++) {
        debug_final_thrust_vector << F(i) << " ";
    }
    RCLCPP_INFO(this->get_logger(),
        "final_thrust_vector: %s", debug_final_thrust_vector.str().c_str()
    );

    const auto thrusts{allocator.solveWrench(F)};

    if(cmd_js_pub)
    {
      std::copy(thrusts.data(), thrusts.data()+dofs, cmd.effort.begin());
      cmd.header.stamp = get_clock()->now();
      cmd_js_pub->publish(cmd);
    }

    if(!cmd_gz_pub.empty())
    {
      static Float64 cmd_gz;
      for(size_t i = 0; i < dofs; ++i)
      {
        cmd_gz.data = thrusts[i];
        cmd_gz_pub[i]->publish(cmd_gz);
      }
    }
}

void ThrusterManagerNode::reset_desired_thrusts_callback(Empty::SharedPtr msg) {
    (void) msg;
    desired_thrusts.clear();
}

// void ThrusterManagerNode::solve(const Wrench &wrench)
// {
//   ThrusterManager::Vector6d F;
//   F(0) = wrench.force.x;
//   F(1) = wrench.force.y;
//   F(2) = wrench.force.z;
//   F(3) = wrench.torque.x;
//   F(4) = wrench.torque.y;
//   F(5) = wrench.torque.z;

//   const auto thrusts{allocator.solveWrench(F)};

//   if(cmd_js_pub)
//   {
//     std::copy(thrusts.data(), thrusts.data()+dofs, cmd.effort.begin());
//     cmd.header.stamp = get_clock()->now();
//     cmd_js_pub->publish(cmd);
//   }

//   if(!cmd_gz_pub.empty())
//   {
//     static Float64 cmd_gz;
//     for(size_t i = 0; i < dofs; ++i)
//     {
//       cmd_gz.data = thrusts[i];
//       cmd_gz_pub[i]->publish(cmd_gz);
//     }
//   }
// }


#include "rclcpp_components/register_node_macro.hpp"

RCLCPP_COMPONENTS_REGISTER_NODE(thruster_manager::ThrusterManagerNode)
