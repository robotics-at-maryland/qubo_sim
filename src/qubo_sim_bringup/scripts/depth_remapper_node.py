#!/usr/bin/env python3

# This node makes depth available under a /depth topic
# by re-publishing the depth component of the private odometry message
# /private/model/Qubo/odometry

# Note: reference for using rclpy in a colcon cmake package
# https://roboticsbackend.com/ros2-package-for-both-python-and-cpp-nodes/
import rclpy

from rclpy.node import Node

from nav_msgs.msg import Odometry

from std_msgs.msg import Float64

class DepthRemapper(Node):
    def __init__(self):
        super().__init__('depth_remapper')
        self.odom_sub = self.create_subscription(
            Odometry,
            "/private/model/Qubo/odometry",
            self.odom_callback,
            10
        )
        self.depth_pub = self.create_publisher(
            Float64,
            "/depth",
            10
        )
    def odom_callback(self, msg):
        # This is a geometry_msgs/PoseWithCovariance
        pose_wcov = msg.pose
        # geometry_msgs/Pose now
        pose = pose_wcov.pose

        depth = pose.position.z

        msg = Float64()
        msg.data = depth

        self.depth_pub.publish(msg)

def main(args=None):
    rclpy.init(args=args)

    depth_remapper = DepthRemapper()

    rclpy.spin(depth_remapper)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    depth_remapper.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()






