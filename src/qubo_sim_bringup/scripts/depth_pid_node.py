#!/usr/bin/env python3

# This node takes in a desired depth on the /desired_depth topic
# and makes Qubo go to that depth via PID

# (Technically, it only does P control, but implementing the I and D part is left as an exercise
# for the reader :) )

# Note: reference for using rclpy in a colcon cmake package
# https://roboticsbackend.com/ros2-package-for-both-python-and-cpp-nodes/
import rclpy

from rclpy.node import Node

from std_msgs.msg import Float64

from geometry_msgs.msg import Wrench, Vector3

class DepthPID(Node):
    def __init__(self):
        super().__init__('depth_pid')

        self.declare_parameter("desired_depth_topic", rclpy.Parameter.Type.STRING)
        self.declare_parameter("thrust_topic", rclpy.Parameter.Type.STRING)

        desired_depth_topic = self.get_parameter("desired_depth_topic").value
        thrust_topic = self.get_parameter("thrust_topic").value

        # TODO: make configurable from launch file
        update_period = 0.1


        self.desired_depth_sub = self.create_subscription(
            Float64,
            desired_depth_topic,
            self.update_desired_depth,
            10
        )
        self.depth_sub = self.create_subscription(
            Float64,
            # TODO: avoid hardcoding?
            "/depth",
            self.update_curr_depth,
            10
        )

        self.thrust_pub = self.create_publisher(
            Wrench,
            thrust_topic,
            10
        )

        self.pub_timer = self.create_timer(update_period, self.pub_wrench)

        self.curr_depth = None
        self.desired_depth = None

    def update_curr_depth(self, msg):
        self.curr_depth = msg.data

    def update_desired_depth(self, msg):
        self.desired_depth = msg.data


    def pub_wrench(self):
        # self.get_logger().info('publishing wrench')
        if self.curr_depth is None or self.desired_depth is None:
            # Wait for the current depth and desired_depth to be populated
            # self.get_logger().info('a depth is not updated')
            return

        # Only force in z direction should be non zero

        fz = self.desired_depth - self.curr_depth

        msg = Wrench(force=Vector3(x=0.0, y=0.0, z=fz), torque=Vector3(x=0.0, y=0.0, z=0.0))

        self.thrust_pub.publish(msg)


def main(args=None):
    rclpy.init(args=args)

    depth_pid = DepthPID()

    rclpy.spin(depth_pid)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    depth_pid.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()






