import os

from ament_index_python.packages import get_package_share_directory

from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.actions import IncludeLaunchDescription
from launch.conditions import IfCondition
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import LaunchConfiguration, PathJoinSubstitution

from launch_ros.actions import Node


def generate_launch_description():
    # Configure ROS nodes for launch
    no_gui = " "
    try:
        if os.environ["GUI"] == 0:
            print("_________________________NO GUI__________________________________")
            no_gui = " -v -r -s --headless-rendering"
    except:
        print("#####################No GUI variable specified defaulting to GUI###########################")
    # Setup project paths
    pkg_project_bringup = get_package_share_directory('qubo_sim_bringup')
    pkg_project_gazebo = get_package_share_directory('qubo_sim_gazebo')
    pkg_project_description = get_package_share_directory('qubo_sim_description')
    pkg_ros_gz_sim = get_package_share_directory('ros_gz_sim')
    # pkg_qubo_controller = get_package_share_directory('qubo_controller')


    # Load the SDF file from "description" package
    # sdf_file  =  os.path.join(pkg_project_description, 'models', 'qubo', 'model.sdf')
    # with open(sdf_file, 'r') as infp:
    #     robot_desc = infp.read()

    sdf_file  =  os.path.join(pkg_project_description, 'models', 'bluerov2_heavy', 'model.sdf')
    with open(sdf_file, 'r') as infp:
        robot_desc = infp.read()

    # Setup to launch the simulator and Gazebo world
    gz_sim = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            os.path.join(pkg_ros_gz_sim, 'launch', 'gz_sim.launch.py')),
        launch_arguments={'gz_args':
            [PathJoinSubstitution([
                pkg_project_gazebo,
                'worlds',
                'nbrf',
                'nbrf.sdf'
            ]),
            # Uncomment this line if you want verbose output from gazebo
            # " -v",
            no_gui
            #" -v -r -s --headless-rendering"
        ]
        }.items(),
    )

    robot_state_publisher = Node(
        package='robot_state_publisher',
        executable='robot_state_publisher',
        name='robot_state_publisher',
        output='both',
        parameters=[
            {'use_sim_time': True},
            {'robot_description': robot_desc},
        ]
    )

    # Bridge ROS topics and Gazebo messages for establishing communication
    bridge = Node(
        package='ros_gz_bridge',
        executable='parameter_bridge',
        parameters=[{
            #'config_file': os.path.join(pkg_project_bringup, 'config', 'sim_bridge.yaml'),
            'config_file': os.path.join(pkg_project_bringup, 'config', 'sim_bridge_bluerov.yaml'),
            'qos_overrides./tf_static.publisher.durability': 'transient_local',
        }],
        output='screen'
    )

    thrust_manager = Node(
        package='thruster_manager',
        executable='thruster_manager_node',
        parameters=[{
            "tam.thruster_prefix": "thruster",
            "publish_joint_state": True,
            'publish_gz_command': True,
        }]
    )

    qubo_controller = Node(
        package='qubo_controller',
        executable="teleop"
    )

    depth_remapper = Node(
        package="qubo_sim_bringup",
        executable="depth_remapper_node.py"
    )

    # Optional nodes

    # TODO: better way to enable/disable these?
    depth_pid = Node(
        package="qubo_sim_bringup",
        executable="depth_pid_node.py",
        parameters=[{
            "desired_depth_topic": "desired_depth",
            "thrust_topic": "wrench",
        }]
    )

    return LaunchDescription([
        depth_remapper,
        gz_sim,
        bridge,
        robot_state_publisher,
        thrust_manager,
        # Optional nodes
        # Anything below can be commented out
        qubo_controller, # teleop
        #depth_pid,      # sample depth PID control
    ])
