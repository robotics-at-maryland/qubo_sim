import rclpy
import pygame
from rclpy.node import Node
from std_msgs.msg import String
from geometry_msgs.msg import Wrench, Vector3

class Teleop(Node):

    def __init__(self):
        super().__init__('keyboard_teleop')
        self.thruster_publisher = self.create_publisher(Wrench, '/wrench', 10)
        timer_period = 0.02  # seconds
        self.teleop = self.create_timer(timer_period, self.teleop_callback)
        self.pygame = pygame
        self.pygame.init()
        self.pygame.display.set_mode([500,500])
        self.pygame.key.set_repeat(10,10)

    def teleop_callback(self):

        fx = fy = fz = tx = ty = tz = 0.0
        linF = 1.2
        rotT = 1.2

        for event in self.pygame.event.get():
            if event.type == self.pygame.KEYDOWN:
                print(event.key)


        keys_pressed = self.pygame.key.get_pressed()

        if keys_pressed[self.pygame.K_w]:
            fx = linF
        elif keys_pressed[self.pygame.K_s]:
            fx = -linF
        else:
            fx = 0.0

        # Note: positive y direction is to the left
        if keys_pressed[self.pygame.K_a]:
            fy = linF
        elif keys_pressed[self.pygame.K_d]:
            fy = -linF
        else:
            fy = 0.0

        if keys_pressed[self.pygame.K_UP]:
            fz = linF
        elif keys_pressed[self.pygame.K_DOWN]:
            fz = -linF
        else:
            fz = 0.0

        if keys_pressed[self.pygame.K_LEFT]:
            tz = rotT
        elif keys_pressed[self.pygame.K_RIGHT]:
            tz = -rotT
        else:
            tz = 0.0

        if keys_pressed[self.pygame.K_j]:
            ty = -rotT
        elif keys_pressed[self.pygame.K_l]:
            ty = rotT
        else:
            ty = 0.0

        if keys_pressed[self.pygame.K_i]:
            tx = -rotT
        elif keys_pressed[self.pygame.K_k]:
            tx = rotT
        else:
            tx = 0.0

        msg = Wrench(force=Vector3(x=fx, y=fy, z=fz), torque=Vector3(x=tx, y=ty, z=tz))

        self.thruster_publisher.publish(msg)

def main(args=None):
    rclpy.init(args=args)

    teleop = Teleop()

    rclpy.spin(teleop)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    teleop.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
