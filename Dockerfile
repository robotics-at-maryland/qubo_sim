FROM osrf/ros:humble-desktop-full

RUN apt update && apt upgrade -y && apt install -y libi2c-dev ros-${ROS_DISTRO}-ros-gz xvfb dbus-x11

RUN dbus-uuidgen > /etc/machine-id

RUN mkdir /ros_ws
RUN mkdir /ros_ws/src

RUN echo 'source /opt/ros/humble/setup.bash' >> /root/.bashrc

ENV GUI=0
WORKDIR /ros_ws/src/
COPY ros_entrypoint.sh /

ENTRYPOINT [ "/ros_entrypoint.sh" ]

