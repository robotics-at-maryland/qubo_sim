#!/bin/bash

set -e 

cd /ros_ws/src/qubo_sim
source /opt/ros/humble/setup.bash
rosdep install --from-paths src --ignore-src -r -i -y --rosdistro ${ROS_DISTRO}
colcon build --cmake-args -DBUILD_TESTING=ON
source install/setup.sh



exec "$@"